---
view: category
lang: en
order: 1      # Order of display in list categories
top: true     # Include category in navigation Top
title: Javascript
description: JavaScript often abbreviated as JS, is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm. 
excerpt: JavaScript often abbreviated as JS, is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.
slug: Javascript
meta:
  - property: og:image
    content: /image-social-share.png
  - name: twitter:image
    content: /image-social-share.png
---