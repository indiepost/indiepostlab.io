# Usage Instructions

The website requires that you resize the images and save them with specific filename for responsivity of the web page. This script helps you with it.

Example :
```
   sudo python resize.py ../.vuepress/public/images/posts/2019/1/python-beginner.png
```

The above script will generate the following files : 
- functional-programming_320.png
- functional-programming_427.png
- functional-programming_524.png
- functional-programming_680.png
