---
view: page
title: Lets talk ! 
description: 
excerpt: 
meta:
  - property: og:image
    content: /image-social-share.png
  - name: twitter:image
    content: /image-social-share.png
---
<div class="survey mongkey surveymongkey--fix">
  <lazy-load tag="iframe" :data="{ src: 'https://www.surveymonkey.com/r/B228RPX', height: 1000 }" />
</div> 

<!-- 
See more in https://github.com/ktquez/vuepress-theme-ktquez#lazy-load
-->


## Contact
* WA(WhatsApp): +62 877-7011-2401
* Telegram : @Maverick_17
* LINE : @ansrama
* Twitter : @weirdwire7

