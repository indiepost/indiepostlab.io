const ads = []

module.exports = {
  languages: {
    label: 'English',
    shortname: 'EN'
  },
  translation: {
    news_title: 'Receive our articles, videos and more directly in your inbox and stay up to date.',
  },
  ads,
  logo: {
    name: 'Logo',
    ext: 'png',
    alt: 'IndiePost Blog'
  },
  share: {
    facebookCaption: '',
    twitterVia: '',
  },
  newsletter: {
    provider: 'mailchimp',
    action: ''
  },
  copy: `2019 © IndiePost Blog Made with  -
        <a href="https://vuepress.vuejs.org/" rel="noopener" target="_blank">
          VuePress
        </a>`,
  footer: {
    nav1: {
      title: 'IndiePost Blog',
      items: [
        {
          label: 'ABOUT',
          path: '/about/'
        },
        {
          label: 'CATEGORIES',
          path: '/categories/'
        },
        {
          label: 'CONTACT',
          path: '/contact/'
        }
      ]
    },
    nav2: {
      title: '',
      items: [
        {
          label: '',
          link: ''
        }
      ]
    }
  },
  social: [
    {
      name: 'twitter',
      link: `https://www.twitter.com/weirdwire7`
    },
    {
      name: 'github',
      link: `https://www.github.com/AnasR7`
    }
  ]
}
