---
view: page
title: Anas Ramadhan 
description: Web developer & OpenSource enthusiast
excerpt: Web developer & OpenSource enthusiast
ctaContact: true
meta:
  - property: og:image
    content: /author/AnasR7.png
  - name: twitter:image
    content: /author/AnasR7.png

---

## About me

Hi! I am Anas Ramadhan, a Web Developer & Opensource enthusiast pursuing my undergraduate studies from UNSIKA (University Singaperbangsa of Karawang).
To know more, take a look at my Curriculum vitae that has been attached below :             
[Resume](/Resume.pdf)
