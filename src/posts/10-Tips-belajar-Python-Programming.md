---
view: post
layout: post
lang: en
author: AnasR7 
title: 10 Tips memulai belajar Python Programming
description: 10 Tips belajar Python Programming untuk kalian yang masih pemula atau yang ingin belajar python programming.
excerpt: 10 Tips belajar Python Programming, untuk kalian yang masih pemula atau ingin belajar python programming.
cover: true
coverAlt: 
categories:
  - Python
  - Tutorial
  - 🇮🇩 Lokal
tags: 
  - Object Oriented
  - Python
  - Tutorial
  - 🇮🇩 Lokal
created_at: 2019-01-20 11:50
updated_at: 2019-01-20 11:50
meta:                                 # If you have cover image
  - property: og:image
    content: /images/posts/2019/1/10-Tips-belajar-Python-Programming.png
  - name: twitter:image
    content: /images/posts/2019/1/10-Tips-belajar-Python-Programming.png

---

___
**L**angkah pertama dalam mempelajari bahasa pemrograman adalah memastikan bahwa Kamu memahami cara belajar dengan baik.
Mempelajari cara belajar merupakan unsur yang sangat penting dalam mempelajari bahasa pemrogramman / *Programming Language* seperti halnya Python.

Mengetahui Tips & Trick dalam mempelajari Programming Language itu sangat penting, karena setiap bahasa pemrogramman / *Programming Language* 
itu cepat berkembang dari waktu ke waktu. Untuk itu IndiePost akan memberikan Tips & Trick supaya kalian bisa menjadi Programming yang Sukses!


## 1. Mempersiapkan Alat/Tools & Python Compiler untuk Belajar.
<iframe src="https://giphy.com/embed/553ZGtulfktgWzQ4Ml" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/CBeebiesHQ-action-553ZGtulfktgWzQ4Ml">via GIPHY</a></p>

Persiapan adalah hal yang sangat penting dalam mempelajari bahasa pemrograman, guna mempermudah kita untuk belajar Python Programming atau bahasa pemrograman Python.
Pertama-tama yang harus kita lakukan adalah Install Python Compiler di komputer kalian, kalian bisa kunjingi website resmi dari Python
[www.python.org](https://python.org), dan untuk toolsnya kalian bisa menggunakan IDLE yang sudah tersedia di python atau bisa menggunakan IDE python seperti 
[Pycharm Community Edition](https://www.jetbrains.com/pycharm/download/), [VSCode](https://code.visualstudio.com/download)
dengan Plugin Python,untuk lebih lengkapnya kalian bisa check melalui website resminya mengenai plugin python di sini 
[Link](https://code.visualstudio.com/docs/languages/python)

## 2. Menggunakan Interactive Shell.
<iframe src="https://giphy.com/embed/2uI6D6l7sRu4iSausG" width="480" height="240" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/retro-2uI6D6l7sRu4iSausG">via GIPHY</a></p>

## 3. Fokus dan Rileks.
<iframe src="https://giphy.com/embed/QHE5gWI0QjqF2" width="480" height="320" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-arkham-footage-QHE5gWI0QjqF2">via GIPHY</a></p>

## 4. Coding Everyday.
<iframe src="https://giphy.com/embed/UcK7JalnjCz0k" width="480" height="360" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/coding-programming-hackny-UcK7JalnjCz0k">via GIPHY</a></p>

## 5. Memahami bug dan solusinya.
<iframe src="https://giphy.com/embed/1xOQlQxrIX4Jw6lBZI" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/bug-night-flight-1xOQlQxrIX4Jw6lBZI">via GIPHY</a></p>

## 6. Membuat Project sebanyak-banyaknya.
<iframe src="https://giphy.com/embed/l46C4JfWleIDjtDJm" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/siliconvalleyhbo-l46C4JfWleIDjtDJm">via GIPHY</a></p>

## 7. Belajar memahami Artikel Bahasa Inggris.
<iframe src="https://giphy.com/embed/l4FGyJpAhygQmJhhC" width="480" height="240" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/vh1-basketball-wives-l4FGyJpAhygQmJhhC">via GIPHY</a></p>

## 8. Ikut Komunitas atau Forum Programming.
<iframe src="https://giphy.com/embed/l396KUuwr8gsPlJKw" width="480" height="268" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/silicon-valley-l396KUuwr8gsPlJKw">via GIPHY</a></p>

## 9. Banyak-banyaklah bertanya.
<iframe src="https://giphy.com/embed/d1E1YlkOTe4IfdNC" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/latenightseth-question-seth-meyers-d1E1YlkOTe4IfdNC">via GIPHY</a></p>

## 10. Berkontribusi dalam OpenSource Project
<iframe src="https://giphy.com/embed/ogcZlWBOqZbJS" width="480" height="352" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/opensource-ogcZlWBOqZbJS">via GIPHY</a></p>

