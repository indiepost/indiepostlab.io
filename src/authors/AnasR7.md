---
view: author
lang: en
title: My profile 
description: 
name: Anas Ramadhan 
nickname: AnasR7 
role: Web Developer / Opensource Addict
avatar: https://avatars1.githubusercontent.com/u/23519166?s=400&u=7afaa654b25543fd5a5c1c22c1348ddde571ddc8&v=4
created_at: 2018-08-22
social:
  - name: twitter
    url: https://twitter.com/weirdwire17
  - name: github
    url: https://github.com/AnasR7
  - name: site
    url: https://indiepost.gitlab.io
meta:
  - property: og:image
    content: /authors/avatar.png
  - name: twitter:image
    content: /authors/avatar.png
---
